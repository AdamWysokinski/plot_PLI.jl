# plot_PLI.jl

NeuroAnalyzer plugin: plot PLI (phase lag index).

This software is licensed under [The 2-Clause BSD License](LICENSE).

## Usage

```julia
p = plot_pli(eeg, eeg, ch1="Fp1", ch2="Fp2", ep1=1, ep2=2)
plot_save(p, file_name="pli.png")
```

![](pli.png)