export plot_pli

"""
    plot_pli(obj1, obj2; <keyword arguments>)

Plot PLI.

# Arguments

- `obj1:NeuroAnalyzer.NEURO`
- `obj2:NeuroAnalyzer.NEURO`
- `ch1::String`: channel name
- `ch2::String`: channel name
- `ep1::Int64`: epoch to plot
- `ep2::Int64`: epoch to plot
- `mono::Bool=false`: use color or grey palette
- `kwargs`: optional arguments for plot() function

# Returns

- `p::Plots.Plot{Plots.GRBackend}`
"""
function plot_pli(obj1::NeuroAnalyzer.NEURO, obj2::NeuroAnalyzer.NEURO; ch1::String, ch2::String, ep1::Int64, ep2::Int64, mono::Bool=false, kwargs...)::Plots.Plot{Plots.GRBackend}

    palette = mono == true ? :grays : :darktest

    pli_value, signal_diff, phase_diff, s1_phase, s2_phase = pli(obj1, obj2, ch1=ch1, ch2=ch2, ep1=ep1, ep2=ep2)
    pli_value = round.(pli_value, digits=2)

    ch1 = get_channel(obj1, ch=ch1)[1]
    ch2 = get_channel(obj2, ch=ch2)[1]
    t = obj1.epoch_time

    p1 = @views Plots.plot(t, obj1.data[ch1, :, ep1], color=:black, lw=0.2)
    p1 = @views Plots.plot!(t, obj2.data[ch2, :, ep2], color=:grey, lw=0.2, title="Signals", legend=false, xlabel="Time [s]", ylabel="Amplitude [μv]")

    p2 = @views Plots.plot(t, signal_diff[1, :, 1], color=:black, lw=0.2, title="Signals difference", legend=false, xlabel="Time [s]", ylabel="Amplitude [μv]")

    p3 = @views Plots.plot(t, s1_phase[1, :, 1], color=:black, lw=0.2)
    p3 = @views Plots.plot!(t, s2_phase[1, :, 1], color=:grey, lw=0.2, title="Phases", legend=false, xlabel="Time [s]", ylabel="Angle [rad]")

    p4 = @views Plots.plot(t, phase_diff[1, :, 1], color=:black, lw=0.2, title="Phases difference", legend=false, xlabel="Time [s]", ylabel="Angle [rad]")

    p5 = @views Plots.plot([0, s1_phase[1, :, 1][1]], [0, 1], projection=:polar, yticks=false, color=:black, lw=0.2, legend=nothing, title="Phases")
    @views for idx in 2:length(phase_diff[1, :, 1])
        p5 = @views Plots.plot!([0, s1_phase[1, :, 1][idx]], [0, 1], projection=:polar, color=:black, lw=0.2)
    end

    p5 = @views Plots.plot!([0, s2_phase[1, :, 1][1]], [0, 1], projection=:polar, yticks=false, color=:grey, lw=0.2, legend=nothing)
    @views for idx in 2:length(phase_diff[1, :, 1])
        p5 = @views Plots.plot!([0, s2_phase[1, :, 1][idx]], [0, 1], projection=:polar, color=:grey, lw=0.2)
    end

    p6 = @views Plots.plot([0, phase_diff[1, :, 1][1]], [0, 1], projection=:polar, yticks=false, color=:black, lw=0.2, legend=nothing, title="Phases difference and PLI = $(pli_value[1])")
    @views for idx in 2:length(phase_diff[1, :, 1])
        p6 = @views Plots.plot!([0, phase_diff[1, :, 1][idx]], [0, 1], projection=:polar, color=:black, lw=0.2)
    end
    
    p = Plots.plot(p1, p2, p3, p4, p5, p6,
                   layout=(3, 2),
                   titlefontsize=8,
                   xlabelfontsize=6,
                   ylabelfontsize=6,
                   xtickfontsize=4,
                   ytickfontsize=4,
                   palette=palette;
                   kwargs...)

    return p

end